package com.mandzhiev.server1.common;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@XmlRootElement
public class Statistic {
    double amountQuantity;
    double amountSum;
    double commissionSum;

    public Statistic() {
    }

    public Statistic(double amountQuantity, double amountSum, double commissionSum) {
        this.amountQuantity = amountQuantity;
        this.amountSum = amountSum;
        this.commissionSum = commissionSum;
    }


    public double getAmountQuantity() {
        return amountQuantity;
    }

    public void setAmountQuantity(double amountQuantity) {
        this.amountQuantity = amountQuantity;
    }

    public double getAmountSum() {
        return amountSum;
    }

    public void setAmountSum(double amountSum) {
        this.amountSum = amountSum;
    }

    public double getCommissionSum() {
        return commissionSum;
    }

    public void setCommissionSum(double commissionSum) {
        this.commissionSum = commissionSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statistic that = (Statistic) o;
        return Double.compare(that.amountQuantity, amountQuantity) == 0 &&
                Double.compare(that.amountSum, amountSum) == 0 &&
                Double.compare(that.commissionSum, commissionSum) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amountQuantity, amountSum, commissionSum);
    }

    @Override
    public String toString() {
        return "Statistic{" +
                "amountQuantity=" + amountQuantity +
                ", amountSum=" + amountSum +
                ", commissionSum=" + commissionSum +
                '}';
    }
}
