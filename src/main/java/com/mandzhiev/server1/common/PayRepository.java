package com.mandzhiev.server1.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface PayRepository extends JpaRepository<Pay, Long> {

    @Query(value = "select sum(p.amount) from Pay p where p.pointOfSale=?1 and p.localDate>?2 group by p.pointOfSale")
    Double pointSumAmount(String pointOfSale, LocalDate date);

    @Query(value = "select sum(p.commission) from Pay p where p.pointOfSale=?1 and p.localDate>?2 group by p.pointOfSale")
    Double pointSumCommission(String pointOfSale, LocalDate date);

    @Query(value = "select count (p.amount) from Pay p where p.pointOfSale=?1 and p.localDate>?2 group by p.pointOfSale")
    Double pointCountAmount(String pointOfSale, LocalDate date);

    @Query(value = "select sum(p.amount) from Pay p where p.localDate=?1 and p.localDate>?2 group by p.localDate")
    Double dateSumAmount(LocalDate date, LocalDate lastUpdate);

    @Query(value = "select sum(p.commission) from Pay p where p.localDate=?1 and p.localDate>?2 group by p.localDate")
    Double dateSumCommission(LocalDate date, LocalDate lastUpdate);

    @Query(value = "select count (p.amount) from Pay p where p.localDate=?2 and p.localDate>?1 group by p.localDate")
    Double dateCountAmount(LocalDate lastUpdate, LocalDate date);
}