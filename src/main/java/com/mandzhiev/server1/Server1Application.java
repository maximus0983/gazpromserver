package com.mandzhiev.server1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class Server1Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        if (args.length != 0) {
            String port = args[0];
            System.out.println(args[0]);
            if (port != null) {
                System.setProperty("server.port", port);
            }
            SpringApplication.run(Server1Application.class);
        }
    }
}