package com.mandzhiev.server1.controllers;

import com.mandzhiev.server1.common.Pay;
import com.mandzhiev.server1.common.PayRepository;
import com.mandzhiev.server1.common.Statistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    private Map<String, Statistic> statisticMap = new HashMap<>();
    private LocalDate lastUpdate;
    @Autowired
    private PayRepository repository;

    @PostMapping("/pay")
    public Pay savePay(@RequestBody Pay pay) {
        pay.setCommission(pay.getAmount() / 100 * 0.15);
        String pointOfSale = pay.getPointOfSale();
        Pay newPay = repository.save(pay);
        updateStatistic(pointOfSale, newPay.getLocalDate());
        return newPay;
    }

    private void updateStatistic(String pointOfSale, LocalDate date) {
        if (lastUpdate == null) {
            lastUpdate = LocalDate.of(2011, 01, 01);
        }
        Double pointCountAmount = repository.pointCountAmount(pointOfSale, lastUpdate);
        Double pointSumAmount = repository.pointSumAmount(pointOfSale, lastUpdate);
        Double pointSumCommission = repository.pointSumCommission(pointOfSale, lastUpdate);
        Statistic statisticOfPoint = new Statistic(pointCountAmount, pointSumAmount, pointSumCommission);
        statisticMap.put(pointOfSale, statisticOfPoint);

        Double dateCountAmount = repository.dateCountAmount(lastUpdate, date);
        Double dateSumAmount = repository.dateSumAmount(date, lastUpdate);
        Double dateSumCommission = repository.dateSumCommission(date, lastUpdate);
        Statistic statisticOfDate = new Statistic(dateCountAmount, dateSumAmount, dateSumCommission);
        statisticMap.put(date.toString(), statisticOfDate);
    }

    @GetMapping("/stat")
    public Map<String, Statistic> statistic() {
        lastUpdate = LocalDate.now();
        return statisticMap;
    }
}